'use strict';
angular.module('screenInstruction', [
  'ionic',
  'ngCordova',
  'ui.router',
])
.config(function ($stateProvider) {
  $stateProvider
    .state('screenInstruction', {
      url: '/screen-instruction',
      templateUrl: 'screen-instruction/templates/instruction.html',
    });
});
