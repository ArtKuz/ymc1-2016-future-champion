'use strict';
angular
  .module('main')
  .controller('MainCtrl', function (Results,
                                    $scope,
                                    $ionicHistory) {
    var vm = this;

    $scope.$on('$ionicView.enter', function() {
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();
    });

    vm.items = [];

    Results.getKeys()
      .then(function(res) {
        if (res.length === 0) vm.showList = false;
        else {
          vm.showList = true;
          Results.getAll()
            .then(function(items) {
              vm.items = items;
            });
        }
      });

  });
