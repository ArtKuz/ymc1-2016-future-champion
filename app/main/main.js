'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'screenInstruction',
  'screenTraining',
  'serviceResults',
])
.config(function($ionicConfigProvider) {
  $ionicConfigProvider.backButton.text('');
})
.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/main');
  $stateProvider
    .state('main', {
      url: '/main',
      cache: false,
      templateUrl: 'main/templates/main.html',
      controller: 'MainCtrl as vm'
    })
});
