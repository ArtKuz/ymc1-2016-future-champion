'use strict';
angular
  .module('screenTraining')
  .controller('TrainingCtrl', function ($scope,
                                        $ionicPlatform,
                                        $cordovaVibration,
                                        $cordovaDeviceMotion,
                                        $interval,
                                        ngAudio,
                                        $ionicPopup,
                                        Results,
                                        $state
  ) {
    var vm = this;

    vm.isTimer = true;

    var pick = ngAudio.load("screen-training/assets/audio/pick.mp3"),
      watch, interval, startPosition,
      startPositionArr = [],
      startSquat = false;

    vm.countSquats = 0;

    $scope.$on('$ionicView.enter', function() {
      vm.isTimer = true;
      vm.timer = 5;
      timeout(5);
    });

    document.addEventListener("deviceready", function () {
      watch = $cordovaDeviceMotion.watchAcceleration({
        frequency: 100
      });
    }, false);

    function timeout(countSeconds) {
      interval = $interval(function(){
        vm.timer = --countSeconds;
        if (vm.timer === 0) {
          vm.isTimer = false;
          $interval.cancel(interval);
          document.addEventListener("deviceready", function() {
            watch.clearWatch();
            watch = $cordovaDeviceMotion.watchAcceleration({
              frequency: 200
            });
            pick.play();
            $cordovaVibration.vibrate(1000);
            startPosition = getAveragePosition(startPositionArr);
            window.plugins.insomnia.keepAwake();
            watch
              .then(
                null,
                function() {
                  $ionicPopup.alert({
                    title: 'Ошибка',
                    template: 'Не удалось запустить акселерометр'
                  });
                },
                function(result) {
                  var x = Math.round(result.x),
                    y = Math.round(result.y);

                  if (!startSquat) {
                    if (startPosition.x <= 4) {
                      squat(startPosition.x, x);
                    } else if (startPosition.y <= 4) {
                      squat(startPosition.y, y);
                    }
                  } else {
                    if (startPosition.x <= 4) {
                      gotUp(startPosition.x, x);
                    } else if (startPosition.y <= 4) {
                      gotUp(startPosition.y, y);
                    }
                  }
                });
          }, false);
        } else if (vm.timer === 1) {
          document.addEventListener("deviceready", function () {
            watch
              .then(
                null,
                function() {
                  $ionicPopup.alert({
                    title: 'Ошибка',
                    template: 'Не удалось запустить акселерометр'
                  });
                },
                function(result) {
                  startPositionArr.push({
                    x: result.x,
                    y: result.y,
                  });
                });
          }, false);
        }
      }, 1000);
    }

    vm.onEndTraining = function() {
      watch.clearWatch();
      $interval.cancel(interval);
      pick.stop();
      pick.unbind();
      document.addEventListener("deviceready", function () {
        window.plugins.insomnia.allowSleepAgain();
      }, false);
      Results.set(moment().format("DD.MM.YYYY HH:mm:ss"), vm.countSquats)
    };

    function getAveragePosition(arr) {
      var x = 0,
        y = 0,
        arrLength = arr.length;

      _.forEach(arr, function(value) {
        x += value.x;
        y += value.y;
      });

      return {
        x: Math.round(x / arrLength),
        y: Math.round(y / arrLength),
      };
    }

    function squat(startValue, value) {
      if (startValue + 7 <= value || startValue - 7 >= value) {
        startSquat = true;
      }
    }

    function gotUp(startValue, value) {
      if (value >= startValue - 2 && value <= startValue + 2) {
        startSquat = false;
        vm.countSquats++;
      }
    }

    $ionicPlatform.registerBackButtonAction(function() {
      vm.onEndTraining();
      $state.go('main');
    }, 101);

  });
