'use strict';
angular.module('screenTraining', [
  'ionic',
  'ngCordova',
  'ui.router',
  'ngAudio',
])
.config(function ($stateProvider) {
  $stateProvider
    .state('screenTraining', {
      url: '/screen-training',
      cache: false,
      templateUrl: 'screen-training/templates/training.html',
      controller: 'TrainingCtrl as vm',
    });
});
