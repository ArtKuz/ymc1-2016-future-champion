'use strict';

angular
  .module('serviceResults')
  .service('Results', function ($q) {
    var _futureСhampionResultsDB = localforage.createInstance({
      name: "futureСhampionResults"
    });

    return {
      getAll: function () {
        return $q.when(_futureСhampionResultsDB.keys())
          .then(function (keys) {
            var events = {};

            _.each(keys, function (key) {
              events[key] = $q.when(_futureСhampionResultsDB.getItem(key))
            });

            return $q.all(events);
          }).then(function (result) {
            return result;
          });
      },

      getKeys: function () {
        return $q.when(_futureСhampionResultsDB.keys())
      },

      get: function (id) {
        return $q.when(_futureСhampionResultsDB.getItem(id.toString()));
      },

      set: function (id, value) {
        var self = this;
        return $q.when(_futureСhampionResultsDB.setItem(id.toString(), value))
          .then(function (res) {
            return self.get(id);
          });
      },

      remove: function (id) {
        return $q.when(_futureСhampionResultsDB.removeItem(id.toString()));
      },

      clear: function () {
        _futureСhampionResultsDB.clear();
      }
    };
  });
