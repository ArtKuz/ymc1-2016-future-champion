'use strict';

describe('module: serviceResults, service: Results', function () {

  // load the service's module
  beforeEach(module('serviceResults'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Results;
  beforeEach(inject(function (_Results_) {
    Results = _Results_;
  }));

  it('should do something', function () {
    expect(!!Results).toBe(true);
  });

});
