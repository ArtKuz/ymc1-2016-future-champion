'use strict';

describe('module: screenTraining, controller: TrainingCtrl', function () {

  // load the controller's module
  beforeEach(module('screenTraining'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var TrainingCtrl;
  beforeEach(inject(function ($controller) {
    TrainingCtrl = $controller('TrainingCtrl');
  }));

  it('should do something', function () {
    expect(!!TrainingCtrl).toBe(true);
  });

});
